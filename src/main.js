import { createApp } from 'vue'
import { createHead } from '@vueuse/head'
import App from './App.vue'
import router from './router/index'
import VueUniversalModal from 'vue-universal-modal'
import 'vue-universal-modal/dist/index.css'


const app = createApp(App)
const head = createHead()

app.use(head)
app.use(router)
app.use(VueUniversalModal, { teleportTarget: '#modals', modalComponemt: 'MyModel' })
app.mount('#app')
