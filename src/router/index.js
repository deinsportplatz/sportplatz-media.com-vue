import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import SoftwareView from '@/views/SoftwareView.vue'
import MarketingView from '@/views/MarketingView.vue'
import ContactView from '@/views/ContactView.vue'
import NewsView from '@/views/NewsView.vue'
import PublisherView from '@/views/PublisherView.vue'
import ImprintView from '@/views/ImprintView.vue'
import JobsView from '@/views/JobsView.vue'
import PrivacyView from '@/views/PrivacyView.vue'
import NewsDetailPage from '@/views/NewsDetailPage.vue'
import PageNotFound from '@/views/PageNotFound.vue'
import BlogView from '@/views/BlogView.vue'
import ResourcesView from '@/views/ResourcesView.vue';

const routes = [
    {
        path: '/',
        name: 'HomeView',
        component: HomeView,
        meta: {
            mainExtraClasses: 'home'
        }
    },
    {
        path: '/software',
        name: 'SoftwareView',
        component: SoftwareView,
        meta: {
            mainExtraClasses: 'software'
        }
    },
    {
        path: '/vermarktung/advertiser',
        name: 'MarketingView',
        component: MarketingView,
        meta: {
            mainExtraClasses: 'marketing'
        }
    },
    {
        path: '/blog',
        name: 'BlogView',
        component: BlogView,
        meta: {
            mainExtraClasses: 'blog'
        }
    },
    {
        path: '/news',
        name: 'NewsView',
        component: NewsView,
        meta: {
            mainExtraClasses: 'news'
        }
    },
    {
        path: '/ressourcen',
        alias: '/resources',
        name: 'ResourceView',
        component: ResourcesView,
        meta: {
            mainExtraClasses: 'resources'
        }
    },
    {
        path: '/news/:id/:slug?',
        name: 'NewsDetailPage',
        component: NewsDetailPage,
        meta: {
            mainExtraClasses: 'news-detail'
        }
    },
    {
        path: '/kontakt',
        name: 'ContactView',
        component: ContactView,
        meta: {
            mainExtraClasses: 'kontakt'
        }
    },
    {
        path: '/vermarktung/publisher',
        name: 'PublisherView',
        component: PublisherView,
        meta: {
            mainExtraClasses: 'publisher'
        }
    },
    {
        path: '/impressum',
        name: 'ImprintView',
        component: ImprintView,
        meta: {
            mainExtraClasses: 'impressum'
        }
    },
    {
        path: '/datenschutz',
        name: 'PrivacyView',
        component: PrivacyView,
        meta: {
            mainExtraClasses: 'datenschutz'
        }
    },
    {
        path: '/jobs',
        name: 'JobsView',
        component: JobsView,
        meta: {
            mainExtraClasses: 'jobs'
        }
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'PageNotFound',
        component: PageNotFound,
        meta: {
            mainExtraClasses: 'not-found'
        }
    }
]

const router = createRouter({
    history: createWebHistory(),
    base: import.meta.env.BASE_URL,
    routes: routes,
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 }
    }
});

export default router;